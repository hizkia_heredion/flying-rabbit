<!doctype html>
<html class="no-js" lang="en">

<?php include_once 'head.php'; ?>

<body>
    <div class="main-content">
        <div id="my_switcher" class="my_switcher">
            <ul>
                <li>
                    <a href="javascript: void(0);" data-theme="light" class="setColor light">
                        <img src="assets/images/about/sun-01.svg" alt="Sun images"><span title="Light Mode">
                            Light</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" data-theme="dark" class="setColor dark">
                        <img src="assets/images/about/vector.svg" alt="Vector Images"><span title="Dark Mode">
                            Dark</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Start Header -->
        <?php include_once 'header.php'; ?>
        <!-- Start Header -->

        <!-- Start Popup Mobile Menu -->
        <?php include_once 'mobilemenu.php'; ?>
        <!-- End Popup Mobile Menu -->

        <!-- Start Sidebar Area  -->
        <?php /* include_once 'sidebar.php'; */ ?>
        <!-- End Sidebar Area  -->

        <!-- Start Page Wrapper   -->
        <main class="page-wrapper">
            <!-- Start Breadcrumb Area -->
            <div class="axil-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                            <div class="inner">
                                <h1 class="title">One of the fastest growing agency</h1>
                                <p>We design and develop web and mobile applications for our clients worldwide.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 order-1 order-lg-2">
                            <div class="thumbnail text-start text-lg-end">
                                <div class="image-group text-end">
                                    <img class="image-1 paralax-image" src="assets/images/slider/white-shape.png"
                                        alt="Slider images">
                                    <img class="image-2 paralax-image" src="assets/images/slider/banner-about.svg"
                                        alt="Slider images">
                                </div>
                                <div class="shape-group">
                                    <div class="shape shape-1">
                                        <i class="icon icon-breadcrumb-1"></i>
                                    </div>
                                    <div class="shape shape-2">
                                        <i class="icon icon-breadcrumb-2"></i>
                                    </div>
                                    <div class="shape shape-3 customOne">
                                        <i class="icon icon-breadcrumb-3"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Breadcrumb Area -->

            <!-- Start Featured Area -->
            <div class="axil-featured-area ax-section-gap bg-color-white">
                <div class="container">
                    <!-- Start Single Feature  -->
                    <div class="row d-flex flex-wrap axil-featured row--40">
                        <div class="col-lg-6 col-xl-6 col-md-12 col-12">
                            <div class="thumb-inner">
                                <div class="thumbnail">
                                    <img class="image w-100" src="assets/images/featured/featured-image-02.jpg"
                                        alt="Featured Images">
                                </div>
                                <div class="shape-group">
                                    <div class="shape">
                                        <i class="icon icon-breadcrumb-2"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6 col-md-12 col-12 mt_md--40 mt_sm--40">
                            <div class="inner">
                                <div class="section-title text-start">
                                    <span class="sub-title extra04-color wow">featured case study</span>
                                    <h2 class="title wow"><a href="single-case-study.html">Building software for world
                                            changers </a></h2>
                                    <p class="subtitle-2 wow">
                                        We are ready support you to make different ability in digital services. as a
                                        game changers, we understand
                                        what market needs, whats on your mind and also we help fully compasionate in
                                        built your design, built your software, built your digital
                                        advertisement also make everything analytic in one hand.
                                    </p>
                                </div>
                                <div class="axil-counterup-area d-flex flex-wrap separator-line-vertical">
                                    <!-- Start Counterup -->
                                    <div class="single-counterup counterup-style-1">
                                        <h3 class="count">15</h3>
                                        <p>ROI increase</p>
                                    </div>
                                    <!-- End Counterup -->

                                    <!-- Start Counterup -->
                                    <div class="single-counterup counterup-style-1">
                                        <h3 class="count counter-k">60</h3>
                                        <p>Monthly website visits</p>
                                    </div>
                                    <!-- End Counterup -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Feature  -->
                </div>
            </div>
            <!-- End Featured Area -->

            <!-- Start Our Service Area  -->
            <div class="axil-service-area ax-section-gap bg-color-lightest">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <span class="sub-title extra08-color wow" data-splitting>our valus</span>
                                <h2 class="title wow" data-splitting>Why should you work with us?</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-12 mt--90 mt_md--40 mt_sm--30">
                            <div class="axil-service-style--3 move-up wow">
                                <div class="icon">
                                    <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                    <div class="text">1</div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="single-service.html">Exceed clients’ and colleagues’ expectations</a></h4>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-12 mt--90 mt_md--40 mt_sm--30">
                            <div class="axil-service-style--3 color-var--2 move-up wow">
                                <div class="icon">
                                    <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                    <div class="text">2</div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="single-service.html">Take ownership and question the status quo in a constructive manner</a></h4>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-12 mt--90 mt_md--40 mt_sm--30">
                            <div class="axil-service-style--3 color-var--3 move-up wow">
                                <div class="icon">
                                    <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                    <div class="text">3</div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="single-service.html">Be brave, curious and experiment – learn from all successes and failures</a></h4>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-12 mt--90 mt_md--40 mt_sm--30">
                            <div class="axil-service-style--3 color-var--4 move-up wow">
                                <div class="icon">
                                    <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                    <div class="text">4</div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="single-service.html">Act in a way that makes all of us proud</a></h4>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-12 mt--90 mt_md--40 mt_sm--30">
                            <div class="axil-service-style--3 color-var--5 move-up wow">
                                <div class="icon">
                                    <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                    <div class="text">5</div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="single-service.html">Build an inclusive, transparent and socially responsible culture</a></h4>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-12 mt--90 mt_md--40 mt_sm--30">
                            <div class="axil-service-style--3 color-var--2 move-up wow">
                                <div class="icon">
                                    <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                    <div class="text">6</div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="single-service.html">Exceed clients’ and colleagues’ expectations</a></h4>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                    </div>
                </div>
            </div>
            <!-- End Our Service Area  -->

            <!-- Start Team Area -->
            <div class="axil-team-area shape-position ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="thumbnail">
                                <div class="image">
                                    <img src="assets/images/introduction.jpg" alt="Team Images">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-xl-5 offset-xl-1 mt_md--40 mt_sm--40">
                            <div class="content">
                                <div class="inner">
                                    <div class="section-title text-start">
                                        <span class="sub-title extra08-color">History</span>
                                        <h2 class="title">Our Company Make A Start</h2>
                                        <p class="subtitle-2 pr--0">
                                        <b>FLYING RABBIT PTE. LTD.</b>
                                        <br>
                                        The Company was incorporated since 2021, Provider of expertise and capabilities in brand, media, digital and specialist communications intended to offer a holistic and integrated approach in Singapore.
                                        <br><br>
                                        The company's communication services include digital creative execution, SEO strategy, near-field communications (NFC) and marketing analytics and enables clients to communicate and build relationships with consumers around their products and brands.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shape-group">
                    <div class="shape shape-1 customOne">
                        <i class="icon icon-shape-06"></i>
                    </div>
                    <div class="shape shape-2">
                        <i class="icon icon-shape-13"></i>
                    </div>
                    <div class="shape shape-3">
                        <i class="icon icon-shape-14"></i>
                    </div>
                </div>
            </div>
            <!-- End Team Area -->

        </main>
        <!-- End Page Wrapper   -->

        <!-- Start Footer Area -->
        <?php include_once 'footer.php'; ?>
        <!-- End Footer Area -->
    </div>
    <!-- JS
============================================ -->

    <?php include_once 'script.php'; ?>

</body>

</html>