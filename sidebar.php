<div class="side-nav">
            <div class="side-nav-inner">
                <!-- Start Search Bar  -->
                <form action="#" class="side-nav-search-form">
                    <div class="form-group search-field">
                        <input type="text" class="search-field" name="search-field" placeholder="Search...">
                        <button class="side-nav-search-btn"><i class="fas fa-search"></i></button>
                    </div>
                </form>
                <!-- End Search Bar  -->

                <!-- Start Side Content  -->
                <div class="side-nav-content">
                    <div class="row ">
                        <!-- Start Left Bar  -->
                        <div class="col-lg-5 col-xl-6 col-12">
                            <ul class="main-navigation">
                                <li><a href="#">Client Service</a></li>
                                <li><a href="#">Activation Service</a></li>
                                <li><a href="#">Media Investment</a></li>
                                <li><a href="#">Data Science</a></li>
                                <li><a href="#">Technology Development</a></li>
                            </ul>
                        </div>
                        <!-- End Left Bar  -->

                        <!-- Start Right Bar  -->
                        <div class="col-lg-7 col-xl-6 col-12">
                            <div class="axil-contact-info-inner">

                                <!-- Start Single Address  -->
                                <div class="axil-contact-info">
                                    <address class="address">
                                        <span class="title">Contact Information</span>
                                        <p>10 Anson Road #27-18<br> International Plaze Singapore</p>
                                    </address>
                                    <address class="address">
                                        <span class="title">We're Available 24/ 7. Call Now.</span>
                                        <p><a class="tel" href="tel:8884562790"><i class="fas fa-phone"></i>(+65) 079 903</a></p>
                                        <p><a class="tel" href="mailto:support@flyingrabbit.co"><i class="fas fa-envelope"></i>support@flyingrabbit.co</a></p>
                                    </address>
                                </div>
                                <!-- End Single Address  -->
                            </div>
                        </div>
                        <!-- End Right Bar  -->
                    </div>
                </div>
                <!-- End Side Content  -->
                <!-- End of .side-nav-inner -->
                <div class="close-sidenav" id="close-sidenav">
                    <button class="close-button"><i class="fal fa-times"></i></button>
                </div>
            </div>
        </div>