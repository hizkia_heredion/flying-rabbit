<!doctype html>
<html class="no-js" lang="en">

<?php include_once 'head.php'; ?>

<body>
    <div class="main-content">
        <div id="my_switcher" class="my_switcher">
            <ul>
                <li>
                    <a href="javascript: void(0);" data-theme="light" class="setColor light">
                        <img src="assets/images/about/sun-01.svg" alt="Sun images"><span title="Light Mode">
                            Light</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" data-theme="dark" class="setColor dark">
                        <img src="assets/images/about/vector.svg" alt="Vector Images"><span title="Dark Mode">
                            Dark</span>
                    </a>
                </li>
            </ul>
        </div>

        <!-- Start Header -->
        <?php include_once 'header.php'; ?>
        <!-- Start Header -->

        <!-- Start Popup Mobile Menu -->
        <?php include_once 'mobilemenu.php'; ?>
        <!-- End Popup Mobile Menu -->

        <!-- Start Sidebar Area  -->
        <?php /* include_once 'sidebar.php'; */ ?>
        <!-- End Sidebar Area  -->

        <!-- Start Breadcrumb Area -->
        <div class="axil-breadcrumb-area breadcrumb-style-default pt--170 pb--70 theme-gradient">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner">
                            <ul class="axil-breadcrumb liststyle d-flex">
                                <li class="axil-breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="axil-breadcrumb-item active" aria-current="page">Contact</li>
                            </ul>
                            <h1 class="axil-page-title">Contact</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="shape-images">
                <i class="shape shape-1 icon icon-bcm-01"></i>
                <i class="shape shape-2 icon icon-bcm-02"></i>
                <i class="shape shape-3 icon icon-bcm-03"></i>
            </div>
        </div>
        <!-- End Breadcrumb Area -->

        <main class="main-wrapper">

            <!-- Start Contact Area  -->
            <div class="axil-contact-area axil-shape-position ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-xl-5 col-12">
                            <div class="contact-form-wrapper">
                                <!-- Start Contact Form -->
                                <div class="axil-contact-form contact-form-style-1">
                                    <h3 class="title">Get Your Needs, Quote Now</h3>

                                    <?php include_once 'contactform.php'; ?>

                                </div>
                                <!-- End Contact Form -->
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-xl-6 offset-xl-1 col-12 mt_md--40 mt_sm--40">
                            <div class="axil-address-wrapper">
                                <!-- Start Single Address  -->
                                <!-- <div class="axil-address wow move-up">
                                    <div class="inner">
                                        <div class="icon">
                                            <i class="fas fa-phone"></i>
                                        </div>
                                        <div class="content">
                                            <h4 class="title">Phone</h4>
                                            <p>Our customer care is open from Mon-Fri, 9:00 am to 6:00 pm</p>
                                            <p><a class="axil-link" href="tel:+65079983">(+65) 079 903</a></p>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- End Single Address  -->
                                <!-- Start Single Address  -->
                                <div class="axil-address wow move-up mt--60 mt_sm--30 mt_md--30">
                                    <div class="inner">
                                        <div class="icon">
                                            <i class="fal fa-envelope"></i>
                                        </div>
                                        <div class="content">
                                            <h4 class="title">Email</h4>
                                            <p>Our support team will get back to in 48-h during standard business hours.
                                            </p>
                                            <p><a class="axil-link"
                                                    href="mailto:support@flyingrabbit.co">support@flyingrabbit.co</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Address  -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shape-group">
                    <div class="shape shape-01">
                        <i class="icon icon-contact-01"></i>
                    </div>
                    <div class="shape shape-02">
                        <i class="icon icon-contact-02"></i>
                    </div>
                    <div class="shape shape-03">
                        <i class="icon icon-contact-03"></i>
                    </div>
                </div>
            </div>
            <!-- End Contact Area  -->

            <!-- Start Office Location  -->
            <div class="axil-office-location-area ax-section-gap bg-color-lightest">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <span class="sub-title extra04-color wow" data-splitting>who we are</span>
                                <h2 class="title wow mb--0" data-splitting>Our office</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row mt--30">
                        <!-- Start Single Location  -->
                        <div class="d-flex justify-content-center col-lg-12 col-md-12 col-sm-6 col-12">
                            <div class="axil-office-location mt--30 wow move-up">
                                <div class="thumbnail">
                                    <img src="assets/images/inner-image/contact/contact-01.jpg" alt="Location Images">
                                </div>
                                <div class="content text-center">
                                    <h4 class="title">Singapore</h4>
                                    <p>10 Anson Road #27-18 <br /> International Plaza </p>
                                    <a class="axil-button btn-transparent" href="https://goo.gl/maps/xn1kQVMr7atxsMUJ9" target="_blank"><span class="button-text">View on
                                            Map</span><span class="button-icon"></span></a>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Location  -->
                    </div>
                </div>
            </div>
            <!-- End Office Location  -->


        </main>

        <!-- Start Footer Area -->
        <?php include_once 'footer.php'; ?>
        <!-- End Footer Area -->
    </div>
    <!-- JS
============================================ -->

    <?php include_once 'script.php'; ?>

</body>

</html>