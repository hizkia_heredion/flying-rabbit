<!doctype html>
<html class="no-js" lang="en">

<?php include_once 'head.php'; ?>

<body>
    <div class="main-content">
        <div id="my_switcher" class="my_switcher">
            <ul>
                <li>
                    <a href="javascript: void(0);" data-theme="light" class="setColor light">
                        <img src="assets/images/about/sun-01.svg" alt="Sun images"><span title="Light Mode"> Light</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" data-theme="dark" class="setColor dark">
                        <img src="assets/images/about/vector.svg" alt="Vector Images"><span title="Dark Mode"> Dark</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Start Header -->
        <?php include_once 'header.php'; ?>
        <!-- Start Header -->

        <!-- Start Popup Mobile Menu -->
        <?php include_once 'mobilemenu.php'; ?>
        <!-- End Popup Mobile Menu -->

        <!-- Start Sidebar Area  -->
        <?php /* include_once 'sidebar.php'; */ ?>
        <!-- End Sidebar Area  -->

        <!-- Start Breadcrumb Area -->
        <div class="axil-breadcrumb-area breadcrumb-style-default pt--170 pb--70 theme-gradient">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner">
                            <ul class="axil-breadcrumb liststyle d-flex">
                                <li class="axil-breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="axil-breadcrumb-item active" aria-current="page">Blog</li>
                            </ul>
                            <h1 class="axil-page-title">Blog</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="shape-images">
                <i class="shape shape-1 icon icon-bcm-01"></i>
                <i class="shape shape-2 icon icon-bcm-02"></i>
                <i class="shape shape-3 icon icon-bcm-03"></i>
            </div>
        </div>
        <!-- End Breadcrumb Area -->
        <!-- Start Page Wrapper -->
        <main class="page-wrappper">
            <!-- Start Blog Area  -->
            <div class="axil-blog-area ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row row--40">
                        <div class="col-lg-8 col-md-12 col-12">
                            <!-- Start Single Blog  -->
                            <div class="axil-blog-list wow move-up">
                                <div class="blog-top">
                                    <h3 class="title"><a href="blog-details.html">An oral history of the AIM away
                                            message (by the people who were there)</a></h3>
                                    <div class="author">
                                        <div class="author-thumb">
                                            <img src="assets/images/blog/author-01.jpg" alt="Blog Author">
                                        </div>
                                        <div class="info">
                                            <h6>Blanche Owens</h6>
                                            <ul class="blog-meta">
                                                <li>Aug 27, 2019</li>
                                                <li>9 min to read</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumbnail">
                                    <a href="blog-details.html">
                                        <img class="w-100" src="assets/images/blog/blog-list-01.jpg" alt="Blog Images">
                                    </a>
                                </div>
                                <div class="content">
                                    <p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel
                                        hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui
                                        sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim
                                        et, imperdiet vitae mauris.</p>
                                    <a class="axil-button btn-large btn-transparent" href="blog-details.html"><span
                                            class="button-text">Read More</span><span class="button-icon"></span></a>
                                </div>
                            </div>
                            <!-- End Single Blog  -->

                            <!-- Start Single Blog  -->
                            <div class="axil-blog-list wow move-up mt--80 mt_md--30 mt_sm--30 mt_lg--50">
                                <div class="blog-top">
                                    <h3 class="title"><a href="blog-details.html">How developers are taking the
                                            guesswork out of animation coding</a></h3>
                                    <div class="author">
                                        <div class="author-thumb">
                                            <img src="assets/images/blog/author-01.jpg" alt="Blog Author">
                                        </div>
                                        <div class="info">
                                            <h6>Blanche Owens</h6>
                                            <ul class="blog-meta">
                                                <li>Aug 27, 2019</li>
                                                <li>9 min to read</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumbnail">
                                    <a href="blog-details.html">
                                        <img class="w-100" src="assets/images/blog/blog-list-02.jpg" alt="Blog Images">
                                    </a>
                                </div>
                                <div class="content">
                                    <p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel
                                        hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui
                                        sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim
                                        et, imperdiet vitae mauris.</p>
                                    <a class="axil-button btn-large btn-transparent" href="blog-details.html"><span
                                            class="button-text">Read More</span><span class="button-icon"></span></a>
                                </div>
                            </div>
                            <!-- End Single Blog  -->

                            <!-- Start Single Blog  -->
                            <div class="axil-blog-list wow move-up mt--80 mt_md--30 mt_sm--30 mt_lg--50">
                                <div class="blog-top">
                                    <h3 class="title"><a href="blog-details.html">You can now listen to the entire
                                            library of Design Better books for free</a></h3>
                                    <div class="author">
                                        <div class="author-thumb">
                                            <img src="assets/images/blog/author-01.jpg" alt="Blog Author">
                                        </div>
                                        <div class="info">
                                            <h6>Blanche Owens</h6>
                                            <ul class="blog-meta">
                                                <li>Aug 27, 2019</li>
                                                <li>9 min to read</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumbnail">
                                    <a href="blog-details.html">
                                        <img class="w-100" src="assets/images/blog/blog-list-03.jpg" alt="Blog Images">
                                    </a>
                                </div>
                                <div class="content">
                                    <p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel
                                        hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui
                                        sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim
                                        et, imperdiet vitae mauris.</p>
                                    <a class="axil-button btn-large btn-transparent" href="blog-details.html"><span
                                            class="button-text">Read More</span><span class="button-icon"></span></a>
                                </div>
                            </div>
                            <!-- End Single Blog  -->

                            <!-- Start Single Blog  -->
                            <div class="axil-blog-list wow move-up mt--80 mt_md--30 mt_sm--30 mt_lg--50">
                                <div class="blog-top">
                                    <h3 class="title"><a href="blog-details.html">Design mentors share 11 tips for
                                            excelling at your first job as a designer</a></h3>
                                    <div class="author">
                                        <div class="author-thumb">
                                            <img src="assets/images/blog/author-01.jpg" alt="Blog Author">
                                        </div>
                                        <div class="info">
                                            <h6>Blanche Owens</h6>
                                            <ul class="blog-meta">
                                                <li>Aug 27, 2019</li>
                                                <li>9 min to read</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumbnail">
                                    <a href="blog-details.html">
                                        <img class="w-100" src="assets/images/blog/blog-list-02.jpg" alt="Blog Images">
                                    </a>
                                </div>
                                <div class="content">
                                    <p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel
                                        hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui
                                        sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim
                                        et, imperdiet vitae mauris.</p>
                                    <a class="axil-button btn-large btn-transparent" href="blog-details.html"><span
                                            class="button-text">Read More</span><span class="button-icon"></span></a>
                                </div>
                            </div>
                            <!-- End Single Blog  -->

                        </div>
                        <div class="col-lg-4 col-md-12 col-12 mt_md--40 mt_sm--40">
                            <!-- Start Blog Sidebar  -->
                            <div class="axil-blog-sidebar">

                                <!-- Start Single Widget  -->
                                <div class="axil-single-widget search">
                                    <h4 class="title mb--30">Search</h4>
                                    <div class="inner">
                                        <form action="#" class="blog-search">
                                            <input type="text" placeholder="Search…">
                                            <button class="search-button"><i class="fal fa-search"></i></button>
                                        </form>
                                    </div>
                                </div>
                                <!-- End Single Widget  -->

                                <!-- Start Single Widget  -->
                                <div class="axil-single-widget category mt--80 mt_sm--30 mt_md--30 mt_lg--40">
                                    <h4 class="title mb--30">Categories</h4>
                                    <div class="inner">
                                        <ul class="category-list">
                                            <li><a href="#">Agency news</a></li>
                                            <li><a href="#">Blog</a></li>
                                            <li><a href="#">Information technology</a></li>
                                            <li><a href="#">New ideas</a></li>
                                            <li><a href="#">Uncategorized</a></li>
                                            <li><a href="#">Marketing & branding</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Single Widget  -->

                                <!-- Start Single Widget  -->
                                <div class="axil-single-widget share mt--80 mt_sm--30 mt_md--30 mt_lg--40">
                                    <div class="inner">
                                        <div class="blog-share d-flex flex-wrap">
                                            <span>Follow:</span>
                                            <ul class="social-list d-flex">
                                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Widget  -->


                                <!-- Start Single Widget  -->
                                <div class="axil-single-widget small-post-wrapper mt--80 mt_sm--30 mt_md--30 mt_lg--40">
                                    <h4 class="title mb--30">Recent post</h4>
                                    <div class="inner">
                                        <!-- Start Related Post  -->
                                        <div class="small-post">
                                            <div class="thumbnail">
                                                <a href="blog-details.html">
                                                    <img src="assets/images/blog/blog-sm-01.jpg" alt="Blog Image">
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h6><a href="blog-details.html">
                                                        Take ownership and question the status quo in.
                                                    </a></h6>
                                                <ul class="blog-meta">
                                                    <li>Aug 27, 2019</li>
                                                    <li>9 min to read</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- End Related Post  -->
                                        <!-- Start Related Post  -->
                                        <div class="small-post">
                                            <div class="thumbnail">
                                                <a href="blog-details.html">
                                                    <img src="assets/images/blog/blog-sm-03.jpg" alt="Blog Image">
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h6><a href="blog-details.html">
                                                        Take ownership and question the status quo in.
                                                    </a></h6>
                                                <ul class="blog-meta">
                                                    <li>Aug 27, 2019</li>
                                                    <li>9 min to read</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- End Related Post  -->
                                        <!-- Start Related Post  -->
                                        <div class="small-post">
                                            <div class="thumbnail">
                                                <a href="blog-details.html">
                                                    <img src="assets/images/blog/blog-sm-01.jpg" alt="Blog Image">
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h6><a href="blog-details.html">
                                                        Take ownership and question the status quo in.
                                                    </a></h6>
                                                <ul class="blog-meta">
                                                    <li>Aug 27, 2019</li>
                                                    <li>9 min to read</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- End Related Post  -->
                                    </div>
                                </div>
                                <!-- End Single Widget  -->

                                <!-- Start Single Widget  -->
                                <div class="axil-single-widget tags mt--80 mt_sm--30 mt_md--30 mt_lg--40">
                                    <h4 class="title mb--30">Tags</h4>
                                    <div class="inner">
                                        <ul class="tags-list">
                                            <li><a href="#">design</a></li>
                                            <li><a href="#">beauty</a></li>
                                            <li><a href="#">camera</a></li>
                                            <li><a href="#">development</a></li>
                                            <li><a href="#">chicago</a></li>
                                            <li><a href="#">chicago</a></li>
                                            <li><a href="#">graphic design</a></li>
                                            <li><a href="#">film production</a></li>
                                            <li><a href="#">culinary</a></li>
                                            <li><a href="#">craft</a></li>
                                            <li><a href="#">web development</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End Single Widget  -->

                            </div>
                            <!-- End Blog Sidebar  -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Blog Area  -->

            <!-- Start Call To Action -->
            <div class="axil-call-to-action-area shape-position ax-section-gap theme-gradient">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="axil-call-to-action">
                                <div class="section-title text-center">
                                    <span class="sub-title extra04-color wow" data-splitting>Let's work together</span>
                                    <h2 class="title wow" data-splitting>Need a successful project?</h2>
                                    <a class="axil-button btn-large btn-transparent" href="#"><span
                                class="button-text">Estimate Project</span><span class="button-icon"></span></a>
                                    <!-- <div class="callto-action">
                                        <span class="text wow" data-splitting>Or call us now</span>
                                        <span class="wow" data-splitting><i class="fal fa-phone-alt"></i> <a href="#">(123)
                                    456 7890</a></span>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shape-group">
                    <div class="shape shape-01">
                        <i class="icon icon-shape-14"></i>
                    </div>
                    <div class="shape shape-02">
                        <i class="icon icon-shape-09"></i>
                    </div>
                    <div class="shape shape-03">
                        <i class="icon icon-shape-10"></i>
                    </div>
                    <div class="shape shape-04">
                        <i class="icon icon-shape-11"></i>
                    </div>
                </div>
            </div>
            <!-- End Call To Action -->

        </main>
        <!-- End Page Wrapper -->
        <!-- Start Footer Area -->
        <?php include_once 'footer.php'; ?>
        <!-- End Footer Area -->
    </div>
    <!-- JS
============================================ -->

    <?php include_once 'script.php'; ?>

</body>

</html>