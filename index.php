<!doctype html>
<html class="no-js" lang="en">

<?php include_once 'head.php'; ?>

<body>
    <div class="main-content">
        <div id="my_switcher" class="my_switcher">
            <ul>
                <li>
                    <a href="javascript: void(0);" data-theme="light" class="setColor light">
                        <img src="assets/images/about/sun-01.svg" alt="Sun images"><span title="Light Mode">
                            Light</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" data-theme="dark" class="setColor dark">
                        <img src="assets/images/about/vector.svg" alt="Vector Images"><span title="Dark Mode">
                            Dark</span>
                    </a>
                </li>
            </ul>
        </div>

        <!-- Start Header -->
        <?php include_once 'header.php'; ?>
        <!-- Start Header -->

        <!-- Start Popup Mobile Menu -->
        <?php include_once 'mobilemenu.php'; ?>
        <!-- End Popup Mobile Menu -->

        <!-- Start Sidebar Area  -->
        <?php /* include_once 'sidebar.php'; */ ?>
        <!-- End Sidebar Area  -->

        <!-- Start Page Wrapper -->
        <main class="page-wrapper">

            <!-- Start Slider Area -->
            <div class="axil-slider-area axil-slide-activation">
                <!-- Start Single Slide -->
                <div
                    class="axil-slide slide-style-default theme-gradient slider-fixed-height d-flex align-items-center paralax-area">
                    <div class="container">
                        <div class="row align-items-center pt_md--60 mt_sm--60">
                            <div class="col-lg-7 col-12 order-2 order-lg-1">
                                <div class="content pt_md--30 pt_sm--30">
                                    <h1 class="axil-display-1 wow slideFadeInUp" data-wow-duration="1s"
                                        data-wow-delay="500ms">We Make Advertising Work Better For People</h1>
                                    <p class="subtitle-3 wow slideFadeInUp" data-wow-duration="1s"
                                        data-wow-delay="800ms">Digital Creative, SEO , NFC, marketing analytics and
                                        <br /> enables clients to communicate consumers around their products and
                                        brands.
                                    </p>
                                </div>

                            </div>
                            <div class="col-lg-5 col-12 order-1 order-lg-2">
                                <div class="topskew-thumbnail-group text-start text-lg-end">
                                    <div class="thumbnail paralax-image">
                                        <img class="light-image" src="assets/images/others/keystoke-image-3.jpg"
                                            alt="Keystoke Images">
                                        <img class="dark-image" src="assets/images/others/keystoke-image-6.png"
                                            alt="Keystoke Images">
                                    </div>
                                    <div class="image-group">
                                        <img class="paralax-image" src="assets/images/others/keystoke-image-4.svg"
                                            alt="Keystoke Images">
                                    </div>
                                    <div class="shape-group">
                                        <div class="shape shape-1 paralax--1">
                                            <i class="icon icon-shape-05"></i>
                                        </div>
                                        <div class="shape shape-2 customOne">
                                            <i class="icon icon-shape-06"></i>
                                        </div>
                                        <div class="shape shape-3 paralax--3">
                                            <i class="icon icon-shape-04"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Slide -->
            </div>
            <!-- End Slider Area -->

            <!-- Start Keys Area-->
            <div class="axil-counterup-area ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row align-items-center">
                        <!-- Start Section Title -->
                        <div class="col-lg-5 col-12">
                            <div class="section-title text-start">
                                <span class="sub-title extra08-color wow" data-splitting>experts in field</span>
                                <h2 class="title wow" data-splitting>What makes us special?</h2>
                                <p class="subtitle-2 wow pr--0" data-splitting>We Believe these 5 keys is our success key in customer services and business</p>
                            </div>
                        </div>
                        <!-- End Section Title -->

                        <!-- Start Counterup Area -->
                        <div class="col-lg-6 offset-xl-1 col-12 mt_md--40 mt_sm--40">
                            <div class="row">
                                <!-- Start Single Counterup  -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="axil-counterup keys-box text-center counter-1 move-up wow">
                                        <div class="icon">
                                        <i class="far fa-list"></i>
                                        </div>
                                        <h3>Client Services</h3>
                                        <p>Through consistent quality, simple. Structure, and outcome focused, premium client service is our primary duty</p>
                                    </div>
                                </div>
                                <!-- End Single Counterup  -->

                                <!-- Start Single Counterup  -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div
                                        class="axil-counterup keys-box text-center color-style-two mt--60 mt_mobile--40 move-up wow">
                                        <div class="icon">
                                            <i class="far fa-megaphone"></i>
                                        </div>
                                        <h3>Activation Services</h3>
                                        <p>We provide activation services and intelligence, enchanced by technology</p>
                                    </div>
                                </div>
                                <!-- End Single Counterup  -->

                                <!-- Start Single Counterup  -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="axil-counterup keys-box text-center color-style-three mt_mobile--40 move-up wow">
                                        <div class="icon">
                                            <i class="far fa-newspaper"></i>
                                        </div>
                                        <h3>Media Investment</h3>
                                        <p>Through modern investment services, we put money against goals that move businesses forward</p>
                                    </div>
                                </div>
                                <!-- End Single Counterup  -->

                                <!-- Start Single Counterup  -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div
                                        class="axil-counterup keys-box text-center color-style-four mt--60 mt_mobile--40 move-up wow">
                                        <div class="icon">
                                            <i class="far fa-analytics"></i>
                                        </div>
                                        <h3>Data Science</h3>
                                        <p>Our evolving data and ID-based solutions are our strategic, competitive advantage</p>
                                    </div>
                                </div>
                                <!-- End Single Counterup  -->

                                <!-- Start Single Counterup  -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div
                                        class="axil-counterup keys-box text-center color-style-five mt--60 mt_mobile--40 move-up wow">
                                        <div class="icon">
                                            <i class="far fa-phone-laptop"></i>
                                        </div>
                                        <h3>Technology Development</h3>
                                        <p>Our unified technology and data approach harness advantage analytics to build the future of media</p>
                                    </div>
                                </div>
                                <!-- End Single Counterup  -->
                            </div>
                        </div>
                        <!-- End Counterup Area -->

                    </div>
                </div>
            </div>
            <!-- End Keys Area -->

            <!-- Start Service Area -->
            <div class="axil-service-area ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <span class="sub-title extra08-color wow" data-splitting>what we can do for you</span>
                                <h2 class="title wow" data-splitting><span>Services we can help you with</span></h2>
                                <p class="subtitle-2 wow" data-splitting>We are professional and thinking forward when
                                    we give you satified digital build and services 24/7 in one time</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Start Single Service -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 move-up wow">
                            <div class="axil-service text-center axil-control paralax-image">
                                <div class="inner">
                                    <div class="icon">
                                        <div class="icon-inner">
                                            <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                            <div class="image-2"><img src="assets/images/icons/icon-04.svg"
                                                    alt="Shape Images"></div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4 class="title wow"><a href="itadvisory-detail.php">Digital & IT Advisory</a>
                                        </h4>
                                        <p class="wow">Our services range from business strategy, planning to analysis
                                            and CIO as a service. We are ready to guide you through the process of
                                            finding the right development strategy you need for your business</p>
                                        <a class="axil-button" data-hover="Learn More" href="itadvisory-detail.php">Learn
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->

                        <!-- Start Single Service -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 move-up wow">
                            <div class="axil-service text-center axil-control paralax-image active">
                                <div class="inner">
                                    <div class="icon gradient-color-02">
                                        <div class="icon-inner">
                                            <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                            <div class="image-2"><img src="assets/images/icons/icon-02.svg"
                                                    alt="Shape Images"></div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4 class="title wow"><a href="advertising-detail.php">Digital Advertising</a></h4>
                                        <p class="wow">
                                            We create 360 digital campaign through social media, influencers activation,
                                            and advertising. Through great digital advertising, your brand is able to
                                            gain a big portion of awareness and engagement from its audience
                                        </p>
                                        <a class="axil-button" href="advertising-detail.php">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->

                        <!-- Start Single Service -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 move-up wow">
                            <div class="axil-service text-center axil-control paralax-image">
                                <div class="inner">
                                    <div class="icon gradient-color-03">
                                        <div class="icon-inner">
                                            <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                            <div class="image-2"><img src="assets/images/icons/icon-03.svg"
                                                    alt="Shape Images"></div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4 class="title wow"><a href="branddevelopment-detail.php">Brand Development</a></h4>
                                        <p class="wow">We help companies to create, develop and strengthening brand’s
                                            growth in market. We design engaging user experience and user interface for
                                            effective and enjoyable use </p>
                                        <a class="axil-button" href="branddevelopment-detail.php">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->

                        <!-- Start Single Service -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 move-up wow">
                            <div class="axil-service text-center axil-control paralax-image">
                                <div class="inner">
                                    <div class="icon gradient-color-04">
                                        <div class="icon-inner">
                                            <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                            <div class="image-2"><img src="assets/images/icons/icon-03.svg"
                                                    alt="Shape Images"></div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4 class="title wow"><a href="webdevelopment-detail.php">Web Development</a></h4>
                                        <p class="wow">We develop iOS, Android, web applications, and e-commerce. From
                                            full-website to microsite, backend to frontend, we provide a full package of
                                            web</p>
                                        <a class="axil-button" href="webdevelopment-detail.php">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->
                    </div>
                </div>
            </div>
            <!-- End Service Area -->

            <!-- Start Featured Area -->
            <div class="axil-featured-area ax-section-gap bg-color-lightest">
                <div class="container axil-featured-activation axil-carousel" data-slick-options='{
                        "spaceBetween": 0, 
                        "slidesToShow": 1, 
                        "slidesToScroll": 1, 
                        "arrows": false, 
                        "infinite": true,
                        "centerMode":true,
                        "dots": true
                    }' data-slick-responsive='[
                        {"breakpoint":769, "settings": {"slidesToShow": 1}},
                        {"breakpoint":756, "settings": {"slidesToShow": 1}},
                        {"breakpoint":481, "settings": {"slidesToShow": 1}}
                    ]'>
                    <!-- Start Single Feature  -->
                    <div class="row d-flex flex-wrap axil-featured row--0">
                        <div class="col-lg-6 col-xl-6 col-md-12 col-12">
                            <div class="thumbnail">
                                <img class="image w-100" src="assets/images/featured/featured-image-01.jpg"
                                    alt="Featured Images">
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-5 col-md-12 col-12 offset-xl-1 mt_md--40 mt_sm--40">
                            <div class="inner">
                                <div class="section-title text-start">
                                    <span class="sub-title extra04-color wow">a featured client</span>
                                    <h2 class="title wow">
                                        boxes, identity & product design for Xiaomi
                                    </h2>
                                    <p class="subtitle-2 wow">
                                        We have a promising partnership with Xiaomi, which helps grow the hardcases box
                                        business for wearable products to meet the target market and attract young,
                                        creative and expressive consumers.
                                    </p>
                                    <!-- <a class="axil-button btn-large btn-transparent" href="#">
                                        <span class="button-text">Read Showcase</span><span class="button-icon"></span>
                                    </a> -->
                                </div>
                                <div class="axil-counterup-area d-flex flex-wrap separator-line-vertical">
                                    <!-- Start Counterup -->
                                    <div class="single-counterup counterup-style-1">
                                        <h3 class="count">15</h3>
                                        <p>ROI increase</p>
                                    </div>
                                    <!-- End Counterup -->

                                    <!-- Start Counterup -->
                                    <div class="single-counterup counterup-style-1">
                                        <h3 class="count counter-k">60</h3>
                                        <p>Monthly website visits</p>
                                    </div>
                                    <!-- End Counterup -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Feature  -->
                    <!-- Start Single Feature  -->
                    <div class="row d-flex flex-wrap axil-featured row--0">
                        <div class="col-lg-6 col-xl-6 col-md-12 col-12">
                            <div class="thumbnail">
                                <img class="image w-100" src="assets/images/featured/featured-image-03.jpg"
                                    alt="Featured Images">
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-5 col-md-12 col-12 offset-xl-1 mt_md--40 mt_sm--40">
                            <div class="inner">
                                <div class="section-title text-start">
                                    <span class="sub-title extra04-color wow">a featured client</span>
                                    <h2 class="title wow">Advertising design, Social media manage & Flyer for OPPO</h2>
                                    <p class="subtitle-2 wow">OPPO is one of the best brands in the mobile phone segment
                                        which is currently very massively circulating in the world market, analytics and
                                        social media marketing are the best choices for cooperation with us</p>
                                    <a class="axil-button btn-large btn-transparent" href="#">
                                        <span class="button-text">Read Showcase</span><span
                                            class="button-icon"></span></a>
                                </div>
                                <div class="axil-counterup-area d-flex flex-wrap separator-line-vertical">
                                    <!-- Start Counterup -->
                                    <div class="single-counterup counterup-style-1">
                                        <h3 class="count">15</h3>
                                        <p>ROI increase</p>
                                    </div>
                                    <!-- End Counterup -->

                                    <!-- Start Counterup -->
                                    <div class="single-counterup counterup-style-1">
                                        <h3 class="count counter-k">40</h3>
                                        <p>Monthly website visits</p>
                                    </div>
                                    <!-- End Counterup -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Feature  -->
                    <!-- Start Single Feature  -->
                    <div class="row d-flex flex-wrap axil-featured row--0">
                        <div class="col-lg-6 col-xl-6 col-md-12 col-12">
                            <div class="thumbnail">
                                <img class="image w-100" src="assets/images/featured/featured-image-04.jpg"
                                    alt="Featured Images">
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-5 col-md-12 col-12 offset-xl-1 mt_md--40 mt_sm--40">
                            <div class="inner">
                                <div class="section-title text-start">
                                    <span class="sub-title extra04-color wow">a featured client</span>
                                    <h2 class="title wow">Design, Social Media & Flyer for ZTE</h2>
                                    <p class="subtitle-2 wow">managing social media engagement is the most important
                                        requirement for ZTE as a leading network technology leader and has many trusted
                                        customers. solutions to build harmonious relationships with corporate consumers
                                        will increase the reach of a wider market</p>
                                    <a class="axil-button btn-large btn-transparent" href="#">
                                        <span class="button-text">Read Showcase</span><span
                                            class="button-icon"></span></a>
                                </div>
                                <div class="axil-counterup-area d-flex flex-wrap separator-line-vertical">
                                    <!-- Start Counterup -->
                                    <div class="single-counterup counterup-style-1">
                                        <h3 class="count">15</h3>
                                        <p>ROI increase</p>
                                    </div>
                                    <!-- End Counterup -->

                                    <!-- Start Counterup -->
                                    <div class="single-counterup counterup-style-1">
                                        <h3 class="count counter-k">40</h3>
                                        <p>Monthly website visits</p>
                                    </div>
                                    <!-- End Counterup -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Feature  -->

                </div>
            </div>
            <!-- End Featured Area -->

            <!-- Start About Us Area -->
            <div class="axil-about-us-area ax-section-gap bg-color-white axil-shape-position">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6 col-md-12 col-12">
                            <div class="axil-about-inner">
                                <div class="section-title text-start">
                                    <span class="sub-title extra08-color move-up wow">about us</span>
                                    <h2 class="title move-up wow">We do design, code <br /> & develop.</h2>
                                    <p class="subtitle-2 mb--50 mb_lg--20 mb_md--20 mb_sm--15 move-up wow">
                                        We have creative idea to delivery any client needs, Web-Development,
                                        Mockup Design, Implementation and Custome Built. Our team have great skill
                                        complexicity skill to make your bussiness growth in digitalization.
                                    </p>
                                    <p class="subtitle-2 move-up wow">
                                        Never stop make any bussiness reach scalable manage and profit.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-5 offset-xl-1 col-md-12 col-12 mt_md--30 mt_sm--30">
                            <div class="contact-form-wrapper">
                                <!-- Start Contact Form -->
                                <div class="axil-contact-form contact-form-style-1">
                                    <h3 class="title">Get Your Needs, Quote Now</h3>
                                    <?php include_once 'contactform.php'; ?>
                                    <!-- <div class="callto-action-wrapper text-center">
                                        <span class="text">Or call us now</span>
                                        <span><i class="fal fa-phone-alt"></i> <a href="#">(+65) 079 903</a></span>
                                    </div> -->
                                </div>
                                <!-- End Contact Form -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Start Shape Group  -->
                <div class="axil-shape-group">
                    <div class="shape shape-1">
                        <i class="icon icon-shape-12"></i>
                    </div>
                    <div class="shape shape-2">
                        <i class="icon icon-shape-03"></i>
                    </div>
                </div>
                <!-- End Shape Group  -->
            </div>
            <!-- End About Us Area -->

            <!-- Start Portfolio Area -->
            <?php /*
            <div class="axil-portfolio-area ax-section-gap bg-color-lightest">
                <div class="container axil-masonary-wrapper">
                    <div class="row align-items-end">
                        <div class="col-lg-5 col-md-12">
                            <div class="section-title">
                                <span class="sub-title extra07-color wow" data-splitting>our projects</span>
                                <h2 class="title wow mb--0" data-splitting>Some of our finest work.</h2>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-12 mt_md--20 mt_sm--20">
                            <div class="messonry-button text-start text-lg-end">
                                <button data-filter="*" class="is-checked"><span class="filter-text">All</span></button>
                                <button data-filter=".cat--1"><span class="filter-text">Branding</span></button>
                                <button data-filter=".cat--2"><span class="filter-text">Web</span></button>
                                <button data-filter=".cat--3"><span class="filter-text">Graphic</span></button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mesonry-list grid-metro3 mt--20">
                                <!-- Start Single Portfolio -->
                                <div
                                    class="portfolio portfolio_style--1 axil-control active portfolio-33-33 cat--1 cat--2">
                                    <div class="inner move-up wow">
                                        <div class="thumb wow paralax-image">
                                            <a href="#"><img
                                                    src="assets/images/portfolio/portfolio-01.jpg"
                                                    alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="#">Creative
                                                        Agency</a></h4>
                                                <span class="category">ios, design</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->

                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 axil-control portfolio-33-33 cat--2">
                                    <div class="inner move-up wow">
                                        <div class="thumb wow paralax-image">
                                            <a href="#"><img
                                                    src="assets/images/portfolio/portfolio-02.jpg"
                                                    alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="#">Rant bike</a></h4>
                                                <span class="category">Branding</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->

                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 portfolio-33-33 cat--3">
                                    <div class="inner move-up wow">
                                        <div class="thumb wow paralax-image">
                                            <a href="#"><img
                                                    src="assets/images/portfolio/portfolio-03.jpg"
                                                    alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="#">All Volees</a></h4>
                                                <span class="category">Web application</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->

                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 portfolio-33-33 cat--1 cat--2">
                                    <div class="inner move-up wow">
                                        <div class="thumb wow paralax-image">
                                            <a href="#"><img
                                                    src="assets/images/portfolio/portfolio-04.jpg"
                                                    alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="#">Larq</a></h4>
                                                <span class="category">Branding, design</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->

                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 portfolio-33-33 cat--3">
                                    <div class="inner move-up wow">
                                        <div class="thumb wow paralax-image">
                                            <a href="#"><img
                                                    src="assets/images/portfolio/portfolio-05.jpg"
                                                    alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="#">Rant bike</a></h4>
                                                <span class="category">Branding</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->

                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 portfolio-33-33 cat--2 cat--3">
                                    <div class="inner move-up wow">
                                        <div class="thumb wow paralax-image">
                                            <a href="#"><img
                                                    src="assets/images/portfolio/portfolio-01.jpg"
                                                    alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="#">Creative
                                                        Agency</a></h4>
                                                <span class="category">ios, design</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->
                            </div>
                            <div class="view-all-portfolio-button mt--60 text-center">
                                <a class="axil-button btn-large btn-transparent btn-xxl" href="#"><span
                                        class="button-text">Discover More Projects</span><span
                                        class="button-icon"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            */ ?>
            <!-- End Portfolio Area -->

            <!-- Start Counterup Area -->
            <div class="axil-counterup-area ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <span class="sub-title extra08-color wow" data-splitting>experts in field</span>
                                <h2 class="title wow" data-splitting>Design startup movement</h2>
                                <p class="subtitle-2 wow" data-splitting>
                                    Every people deserve quality service and gather thousands of customers.
                                    Unlimited scope of design make everything good in one hand.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Start Counterup Area -->
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                            <div class="axil-counterup mt--60 text-center counter-first move-up wow">
                                <div class="icon">
                                    <img src="assets/images/counterup/shape-01.png" alt="Shape Images">
                                </div>
                                <h3 class="count">15</h3>
                                <p>Years of operation</p>
                            </div>
                        </div>
                        <!-- End Counterup Area -->

                        <!-- Start Counterup Area -->
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                            <div class="axil-counterup mt--60 text-center counter-second move-up wow">
                                <div class="icon">
                                    <img src="assets/images/counterup/shape-02.png" alt="Shape Images">
                                </div>
                                <h3 class="count">360</h3>
                                <p>Projects deliverd</p>
                            </div>
                        </div>
                        <!-- End Counterup Area -->

                        <!-- Start Counterup Area -->
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                            <div
                                class="axil-counterup mt--60 mt_md--30 mt_sm--30 text-center counter-third move-up wow">
                                <div class="icon">
                                    <img src="assets/images/counterup/shape-03.png" alt="Shape Images">
                                </div>
                                <h3 class="count">600</h3>
                                <p>Specialist</p>
                            </div>
                        </div>
                        <!-- End Counterup Area -->

                        <!-- Start Counterup Area -->
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                            <div class="axil-counterup mt--60 text-center counter-four move-up wow">
                                <div class="icon">
                                    <img src="assets/images/counterup/shape-04.png" alt="Shape Images">
                                </div>
                                <h3 class="count">64</h3>
                                <p>Years of operation</p>
                            </div>
                        </div>
                        <!-- End Counterup Area -->

                    </div>
                </div>
            </div>
            <!-- End Counterup Area -->

            <!-- Start Testimonial Area -->
            <?php /*
            <div class="axil-testimonial-area ax-section-gap bg-color-lightest">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <span class="sub-title extra05-color wow" data-splitting>testimonials</span>
                                <h2 class="title wow" data-splitting>From getting started</h2>
                                <p class="subtitle-2 wow" data-splitting>
                                    Trust from partners is our priority
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-activation">
                        <div class="row axil-testimonial-single">

                            <!-- Start Single Testimonial -->
                            <div class="col-lg-6 mt--60 mt_sm--30 mt_md--30">
                                <div class="axil-testimonial testimonial axil-control active move-up wow">
                                    <div class="inner">
                                        <div class="clint-info-wrapper">
                                            <div class="thumb">
                                                <img src="assets/images/testimonial/client-01.jpg" alt="Clint Images">
                                            </div>
                                            <div class="client-info">
                                                <h4 class="title">Martha Maldonado</h4>
                                                <span>Marketing and Strategy Director @ Xiaomi</span>
                                            </div>
                                        </div>
                                        <div class="description">
                                            <p class="subtitle-3">
                                                The product design that we entrusted to the flying rabbit was well
                                                executed
                                                Good Service, Good Quality Performance
                                            </p>
                                            <span class="fw-light text-grey float-end" href="#">23 June 2021</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Testimonial -->

                            <!-- Start Single Testimonial -->
                            <div class="col-lg-6 mt--60 mt_sm--30 mt_md--30">
                                <div class="axil-testimonial axil-control testimonial move-up wow">
                                    <div class="inner">
                                        <div class="clint-info-wrapper">
                                            <div class="thumb">
                                                <img src="assets/images/testimonial/client-02.jpg" alt="Clint Images">
                                            </div>
                                            <div class="client-info">
                                                <h4 class="title">Ming Hao</h4>
                                                <span>Sales Executive @ ZTE</span>
                                            </div>
                                        </div>
                                        <div class="description">
                                            <p class="subtitle-3">
                                                I feel great experience and support, never hesitate to get the maximum
                                                benefit. Thank you
                                            </p>
                                            <span class="fw-light text-grey float-end" href="#">23 June 2021</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Testimonial -->

                        </div>
                    </div>
                </div>
            </div>
            */ ?>
            <!-- End Testimonial Area -->

            <!-- Start Team Area -->
            <div class="axil-team-area shape-position ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="thumbnail">
                                <div class="image">
                                    <img src="assets/images/team/team-group.png" alt="Team Images">
                                </div>
                                <div class="total-team-button">
                                    <a href="team.html"><span>20+</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-xl-5 offset-xl-1 mt_md--40 mt_sm--40">
                            <div class="content">
                                <div class="inner">
                                    <div class="section-title text-start">
                                        <span class="sub-title extra08-color wow" data-splitting>our team</span>
                                        <h2 class="title wow" data-splitting>Alone we can do so little; together we can
                                            do so much.</h2>
                                        <p class="subtitle-2 wow pr--0" data-splitting>
                                            we work sincerely to provide the best impact on your business needs, our
                                            team has a good sense to provide the best solution for your needs
                                        </p>
                                        <div class="axil-button-group mt--40">
                                            <a class="axil-button btn-large btn-transparent" href="about.php"><span
                                                    class="button-text">Our Team</span><span
                                                    class="button-icon"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shape-group">
                    <div class="shape shape-1 customOne">
                        <i class="icon icon-shape-06"></i>
                    </div>
                    <div class="shape shape-2">
                        <i class="icon icon-shape-13"></i>
                    </div>
                    <div class="shape shape-3">
                        <i class="icon icon-shape-14"></i>
                    </div>
                </div>
            </div>
            <!-- End Team Area -->

            <!-- Start Pricing Table Area -->
            <?php /*
            <div class="axil-pricing-table-area pricing-shape-position ax-section-gap bg-color-lightest">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <span class="sub-title extra04-color wow" data-splitting>pricing plan</span>
                                <h2 class="title wow" data-splitting>From getting started </h2>
                                <p class="subtitle-2 wow" data-splitting>
                                    We help you to manage eficient cost for your needs
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row mt--20 row--30 mt_sm--0">
                        <!-- Start Single pricing table -->
                        <div class="col-lg-5 col-md-6 col-sm-12 col-12 offset-lg-1">
                            <div class="axil-pricing-table mt--40 active move-up wow">
                                <div class="axil-pricing-inner">
                                    <div class="pricing-header">
                                        <h4>Small Business</h4>
                                        <p>A beautiful, simple website</p>
                                        <div class="price-wrapper">
                                            <div class="price">
                                                <h2 class="currency">$</h2>
                                                <h2 class="heading headin-h3">46</h2>
                                                <span>/month</span>
                                            </div>
                                            <div class="date-option">
                                                <select>
                                                    <option>Yearly</option>
                                                    <option>Monthly</option>
                                                </select>
                                            </div>
                                        </div>
                                        <span class="subtitle">100% money back guarantee</span>
                                    </div>
                                    <div class="pricing-body">
                                        <div class="inner">
                                            <ul class="list-style">
                                                <li>10 Pages Responsive Website</li>
                                                <li>5 PPC Campaigns</li>
                                                <li>10 SEO Keywords</li>
                                                <li>5 Facebook Camplaigns</li>
                                                <li>2 Video Camplaigns</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single pricing table -->

                        <!-- Start Single pricing table -->
                        <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                            <div class="axil-pricing-table mt--40 mt_sm--60 move-up wow">
                                <div class="axil-pricing-inner">
                                    <div class="pricing-header">
                                        <h4>Corporate Business</h4>
                                        <p>A beautiful, simple website</p>
                                        <div class="price-wrapper">
                                            <div class="price">
                                                <h2 class="currency">$</h2>
                                                <h2 class="heading headin-h3">199</h2>
                                                <span>/month</span>
                                            </div>
                                            <div class="date-option">
                                                <select>
                                                    <option>Yearly</option>
                                                    <option>Monthly</option>
                                                </select>
                                            </div>
                                        </div>
                                        <span class="subtitle">100% money back guarantee</span>
                                    </div>
                                    <div class="pricing-body">
                                        <div class="inner">
                                            <ul class="list-style">
                                                <li>50 Pages Responsive Website</li>
                                                <li>Unlimited PPC Campaigns</li>
                                                <li>Unlimited SEO Keywords</li>
                                                <li>Unlimited Facebook Camplaigns</li>
                                                <li>Unlimited Video Camplaigns</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single pricing table -->

                        <div class="col d-flex justify-content-center mt-5">
                            <div class="axil-button-group mt--40">
                                <a class="axil-button btn-large btn-transparent" href="pricing.php"><span
                                        class="button-text">More Pricing</span><span class="button-icon"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shape-group">
                    <div class="shape">
                        <i class="icon icon-shape-15"></i>
                    </div>
                </div>
            </div>
            */?>
            <!-- End Pricing Table Area -->

            <!-- Start Brand Area -->
            <div class="axil-brand-area ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-xl-4 col-lg-4 col-md-8 col-12">
                            <div class="section-title">
                                <span class="sub-title extra06-color wow" data-splitting>top clients</span>
                                <h2 class="title wow" data-splitting>We’ve built solutions for...</h2>
                                <p class="subtitle-2 wow" data-splitting>We gain trust from top tier client's and
                                    continuiting bussiness</p>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 mt_md--40 mt_sm--40">
                            <div class="axil-brand-logo-wrapper">
                                <ul class="brand-list liststyle d-flex flex-wrap justify-content-center">
                                    <li><a href="#">
                                            <img src="assets/images/brand/xiaomi.png" class="img-fluid p-5" alt="">
                                        </a></li>
                                    <li><a href="#">
                                            <img src="assets/images/brand/oppo.png" class="img-fluid p-5" alt="">
                                        </a></li>
                                    <li><a href="#">
                                            <img src="assets/images/brand/zte.png" class="img-fluid p-5" alt="">
                                        </a></li>
                                    <li><a href="#">
                                            <img src="assets/images/brand/hoyoverse.png" class="img-fluid p-5" alt="">
                                        </a></li>
                                    <li><a href="#">
                                            <img src="assets/images/brand/genshin impact.png" class="img-fluid p-5"
                                                alt="">
                                        </a></li>
                                    <li><a href="#">
                                            <img src="assets/images/brand/ctrip.png" class="img-fluid p-5" alt="">
                                        </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Brand Area -->

            <!-- Start Blog Area -->
            <?php /*
            <div class="axil-blog-area ax-section-gap bg-color-lightest">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <span class="sub-title extra04-color">what's going on</span>
                                <h2 class="title">Latest stories</h2>
                                <p class="subtitle-2">
                                    we have news about our technology, design and popular topic's
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row blog-list-wrapper mt--20">
                        <!-- Start Blog Area -->
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="axil-blog axil-control mt--40 active move-up wow">
                                <div class="content">
                                    <div class="content-wrap paralax-image">
                                        <div class="inner">
                                            <span class="category">Economics</span>
                                            <h5 class="title"><a href="blog-details.html">China's economy slows in May,
                                                    more stimulus expected</a></h5>
                                            <p>
                                                The economic rebound seen earlier this year has lost momentum in the
                                                second quarter, prompting China's central bank
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumbnail">
                                    <div class="image">
                                        <img src="assets/images/blog/blog-01.jpg" alt="Blog images">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Blog Area -->

                        <!-- Start Blog Area -->
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="axil-blog axil-control mt--40 move-up wow">
                                <div class="content">
                                    <div class="content-wrap paralax-image">
                                        <div class="inner">
                                            <span class="category">Market</span>
                                            <h5 class="title">
                                                <a href="blog-details.html">
                                                    EU regulators checking if Vivendi jumped gun in Lagardere
                                                    acquisition
                                                </a>
                                            </h5>
                                            <p>
                                                EU antitrust regulators are checking whether French media conglomerate
                                                Vivendi (VIV.PA) closed its acquisition
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumbnail">
                                    <div class="image">
                                        <img src="assets/images/blog/blog-02.jpg" alt="Blog images">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Blog Area -->
                    </div>
                </div>
            </div>
            */?>
            <!-- End Blog Area -->

            <!-- Start Call To Action  -->
            <!-- Start Call To Action -->
            <div class="axil-call-to-action-area shape-position ax-section-gap theme-gradient">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="axil-call-to-action">
                                <div class="section-title text-center">
                                    <span class="sub-title extra04-color wow" data-splitting>Let's work together</span>
                                    <h2 class="title wow" data-splitting>Need a successful project?</h2>
                                    <!-- <div class="callto-action">
                                        <span class="text wow" data-splitting>call us now</span>
                                        <span class="wow" data-splitting><i class="fal fa-phone-alt"></i> <a
                                                href="#">(+65) 079 903</a></span>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shape-group">
                    <div class="shape shape-01">
                        <i class="icon icon-shape-14"></i>
                    </div>
                    <div class="shape shape-02">
                        <i class="icon icon-shape-09"></i>
                    </div>
                    <div class="shape shape-03">
                        <i class="icon icon-shape-10"></i>
                    </div>
                    <div class="shape shape-04">
                        <i class="icon icon-shape-11"></i>
                    </div>
                </div>
            </div>
            <!-- End Call To Action -->
            <!-- End Call To Action  -->

        </main>
        <!-- End Page Wrapper -->

        <!-- Start Footer Area -->
        <?php include_once 'footer.php'; ?>
        <!-- End Footer Area -->
    </div>

    <!-- JS
============================================ -->

    <?php include_once 'script.php'; ?>
</body>

</html>