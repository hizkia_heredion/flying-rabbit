

<footer class="axil-footer footer-default theme-gradient-2">
            <div class="bg_image--2">
                <!-- Start Social Icon -->
                <!-- <div class="ft-social-icon-wrapper ax-section-gapTop">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="ft-social-share d-flex justify-content-center liststyle flex-wrap mb-5">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div> -->
                </div>
                <!-- End Social Icon -->
                <!-- Start Footer Top Area -->
                <?php /*
                <div class="footer-top ax-section-gap">
                    <div class="container">
                        <div class="row">
                            <!-- Start Single Widget -->
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="footer-widget-item axil-border-right">
                                    <h2>Get in touch!</h2>
                                    <p>We give you latest offer, promo and good consulting in quick</p>
                                        <div class="axil-newsletter">
                                            <form class="newsletter-form" action="#">
                                                <input type="email" placeholder="Email">
                                                <a class="axil-button btn-transparent" href="#"><span class="button-text">Subscribe</span><span class="button-icon"></span></a>
                                            </form>
                                        </div>
                                </div>
                            </div>
                            <!-- End Single Widget -->

                            <!-- Start Single Widget -->
                            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mt_mobile--30">
                                <div class="footer-widget-item">
                                    <h6 class="title">IT Services</h6>
                                    <div class="footer-menu-container">
                                        <ul class="ft-menu liststyle link-hover">
                                            <li><a href="#">Website Content</a></li>
                                            <li><a href="#">Website Design</a></li>
                                            <li><a href="#">Email Marketing</a></li>
                                            <li><a href="#">Youtube</a></li>
                                            <li><a href="#">Social Media</a></li>
                                            <li><a href="#">SEO</a></li>
                                            <li><a href="#">Blogging</a></li>
                                            <li><a href="#">Display Ads</a></li>
                                            <li><a href="#">Online PR</a></li>
                                            <li><a href="#">Viral</a></li>
                                            <li><a href="#">Podcasting</a></li>
                                            <li><a href="#">Pay Per Click</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Widget -->

                            <!-- Start Single Widget -->
                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 mt_lg--30 mt_md--30 mt_sm--30">
                                <div class="footer-widget-item">
                                    <h6 class="title">Digital Advertising</h6>
                                    <div class="footer-menu-container">
                                        <ul class="ft-menu liststyle link-hover">
                                            <li><a href="#">Always On+ Campaigns</a></li>
                                            <li><a href="#">Online Activation</a></li>
                                            <li><a href="#">SEO: Organic Search</a></li>
                                            <li><a href="#">Paid Search: Adwords</a></li>
                                            <li><a href="#">Social Media Marketing</a></li>
                                            <li><a href="#">Email Marketing</a></li>
                                            <li><a href="#">Multi-channel Analytics</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Widget -->

                            <!-- Start Single Widget -->
                            <div class="col-xl-1 col-lg-6 col-md-6 col-sm-6 col-12 mt_lg--30 mt_md--30 mt_sm--30">
                                <div class="footer-widget-item widget-support">
                                    <h6 class="title">Support</h6>
                                    <div class="footer-menu-container">
                                        <ul class="ft-menu liststyle link-hover">
                                            <li><a href="contact.php">Contact</a></li>
                                            <li><a href="privacypolicy.php">Privacy Policy</a></li>
                                            <li><a href="#">Sitemap</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Widget -->
                        </div>
                    </div>
                </div>
                */ ?>
                <!-- End Footer Top Area -->
                <!-- Start Copyright -->
                <div class="copyright copyright-default">
                    <div class="container">
                        <div class="row row--0 ptb--20 axil-basic-thine-line">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="inner text-center text-md-start">
                                    <p>© 2021. All rights reserved by Flying Rabbit.</p>
                                </div>
                            </div>
                            <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="quick-contact">
                                    <ul class="link-hover d-flex justify-content-center justify-content-md-end liststyle">
                                        <li><a data-hover="Privacy Policy" href="privacypolicy.php">Privacy Policy</a></li>
                                        <li><a href="#">Terms of Use</a></li>
                                    </ul>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- End Copyright -->
            </div>
        </footer>