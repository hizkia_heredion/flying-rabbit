<!doctype html>
<html class="no-js" lang="en">

<?php include_once 'head.php'; ?>

<body>
    <div class="main-content">

        <div id="my_switcher" class="my_switcher">
            <ul>
                <li>
                    <a href="javascript: void(0);" data-theme="light" class="setColor light">
                        <img src="assets/images/about/sun-01.svg" alt="Sun images"><span title="Light Mode"> Light</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" data-theme="dark" class="setColor dark">
                        <img src="assets/images/about/vector.svg" alt="Vector Images"><span title="Dark Mode"> Dark</span>
                    </a>
                </li>
            </ul>
        </div>

        <!-- Start Header -->
        <?php include_once 'header.php'; ?>
        <!-- Start Header -->

        <!-- Start Popup Mobile Menu -->
        <?php include_once 'mobilemenu.php'; ?>
        <!-- End Popup Mobile Menu -->

        <!-- Start Sidebar Area  -->
        <?php /* include_once 'sidebar.php'; */ ?>
        <!-- End Sidebar Area  -->

        <div class="main-wrapper">
            <!-- Start Breadcrumb Area -->
            <div class="axil-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--20">
                            <div class="inner">
                                <h2 class="title">Digital Advertising</h2>
                                <p>Amplify Your Presence, Maximize Your Reach.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 order-1 order-lg-2">
                            <div class="thumbnail text-start text-lg-end">
                                <div class="image-group">
                                    <img class="image-1 paralax-image" src="assets/images/slider/single-service-02.png" alt="Slider images">
                                    <img class="image-2 paralax-image" src="assets/images/slider/single-service-01.svg" alt="Slider images">
                                </div>
                                <div class="shape-group">
                                    <div class="shape shape-1">
                                        <i class="icon icon-breadcrumb-1"></i>
                                    </div>
                                    <div class="shape shape-2">
                                        <i class="icon icon-breadcrumb-2"></i>
                                    </div>
                                    <div class="shape shape-3 customOne">
                                        <i class="icon icon-breadcrumb-3"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Breadcrumb Area -->

            <!-- Start About Area  -->
            <div class="axil-about-area ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-xl-5 col-md-12 col-12">
                            <div class="contact-form-wrapper">
                                <!-- Start Contact Form -->
                                <div class="axil-contact-form contact-form-style-1">
                                    <h3 class="title">Get Your Needs, Quote Now</h3>
                                    <form id="contact-form" method="POST" action="mail.php">
                                        <div class="form-group">
                                            <input name="con_name" type="text">
                                            <label>Name</label>
                                            <span class="focus-border"></span>
                                        </div>
                                        <div class="form-group">
                                            <input name="con_email" type="email">
                                            <label>Email</label>
                                            <span class="focus-border"></span>
                                        </div>
                                        <div class="form-group">
                                            <input type="text">
                                            <label>Phone</label>
                                            <span class="focus-border"></span>
                                        </div>
                                        <div class="form-group">
                                            <textarea name="con_message"></textarea>
                                            <label>Your message</label>
                                            <span class="focus-border"></span>
                                        </div>

                                        <div class="form-group">
                                            <input type="submit" value="Send message">
                                            <p class="form-messege"></p>
                                        </div>
                                    </form>
                                    <!-- <div class="callto-action-wrapper text-center">
                                        <span class="text">Or call us now</span>
                                        <span><i class="fal fa-phone-alt"></i> <a href="tel:+65079983">(+65) 079 983</a></span>
                                    </div> -->
                                    <!-- Start Shape Group  -->
                                    <div class="shape-group">
                                        <div class="shape shape-01">
                                            <i class="icon icon-shape-07"></i>
                                        </div>
                                        <div class="shape shape-02">
                                            <i class="icon icon-shape-16"></i>
                                        </div>
                                    </div>
                                    <!-- End Shape Group  -->
                                </div>
                                <!-- End Contact Form -->
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-5 offset-xl-2 col-md-12 col-12 mt_md--40 mt_sm--40">
                            <div class="axil-about-inner">
                                <div class="section-title text-start">
                                    <span class="sub-title extra08-color">About us</span>
                                    <h2 class="title">why Digital Advertising <br /> matters?</h2>
                                        <p class="subtitle-2">
                                        In the crowded digital marketplace, standing out is paramount. Our digital advertising services aim to elevate your brand, engaging your audience with compelling and targeted ad campaigns. Using advanced analytics and creative strategies, we connect your products or services to the right people, at the right time, in the right way.
                                        </p>
                                </div>
                                <!-- Start Accordion Area  -->
                                <?php /*
                                <div id="accordion" class="axil-accordion mt--15 mt_md--15 mt_sm--15">
                                    <!-- Start Single Card  -->
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i
                                                        class="fal fa-compress-wide"></i><span>Strategy</span>
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-bs-parent="#accordion">
                                            <div class="card-body">
                                                Tolong bantu wording....
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Single Card  -->

                                    <!-- Start Single Card  -->
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i
                                                        class="fal fa-code"></i><span>Design</span>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-bs-parent="#accordion">
                                            <div class="card-body">
                                                Tolong bantu wording....
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Single Card  -->

                                    <!-- Start Single Card  -->
                                    <div class="card">
                                        <div class="card-header" id="headingThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i
                                                        class="fal fa-globe"></i><span>Development</span>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-bs-parent="#accordion">
                                            <div class="card-body">
                                                    Tolong bantu wording....
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Single Card  -->
                                </div>
                            </div>
                            */?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End About Area  -->

            <!-- Start Working Process  -->
            <?php /*
            <div class="axil-working-process-area ax-section-gap theme-gradient-4">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center mb--100 mb_sm--40 mb_md--40">
                                <span class="sub-title extra08-color wow" data-splitting>process</span>
                                <h2 class="title wow" data-splitting>Our logo design process</h2>
                                <p class="subtitle-2 wow" data-splitting>Our comprehensive logo design strategy ensures
                                    a
                                    perfectly crafted
                                    logo for your business.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">

                            <!-- Start Working Process  -->
                            <div class="axil-working-process mb--100 mb_md--50 mb_sm--40">
                                <div class="thumbnail">
                                    <div class="image paralax-image">
                                        <img src="assets/images/process/process-01.jpg" alt="Process Images">
                                    </div>
                                </div>
                                <div class="content">
                                    <div class="inner">
                                        <div class="section-title">
                                            <span class="process-step-number">1</span>
                                            <span class="sub-title extra04-color">our four step
                                                process</span>
                                            <h2 class="title">Discover</h2>
                                            <p class="subtitle-2">Donec metus lorem, vulputate at sapien
                                                sit amet, auctor
                                                iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit.
                                                Aliquam
                                                tristique libero at dui sodales, et placerat orci lobortis. Maecenas
                                                ipsum
                                                neque, elementum id dignissim et, imperdiet vitae mauris.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Working Process  -->

                            <!-- Start Working Process  -->
                            <div class="axil-working-process mb--100 text-start mb_md--50 mb_sm--40">
                                <div class="content order-2 order-lg-1">
                                    <div class="inner">
                                        <div class="section-title">
                                            <span class="process-step-number">2</span>
                                            <span class="sub-title extra05-color">our four step
                                                process</span>
                                            <h2 class="title">Prototype</h2>
                                            <p class="subtitle-2">Donec metus lorem, vulputate at sapien
                                                sit amet, auctor
                                                iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit.
                                                Aliquam
                                                tristique libero at dui sodales, et placerat orci lobortis. Maecenas
                                                ipsum
                                                neque, elementum id dignissim et, imperdiet vitae mauris.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumbnail order-1 order-lg-2">
                                    <div class="image paralax-image">
                                        <img src="assets/images/process/process-02.jpg" alt="Process Images">
                                    </div>
                                </div>
                            </div>
                            <!-- End Working Process  -->

                            <!-- Start Working Process  -->
                            <div class="axil-working-process mb--100 mb_md--50 mb_sm--40">
                                <div class="thumbnail">
                                    <div class="image paralax-image">
                                        <img src="assets/images/process/process-03.jpg" alt="Process Images">
                                    </div>
                                </div>
                                <div class="content">
                                    <div class="inner">
                                        <div class="section-title">
                                            <span class="process-step-number">3</span>
                                            <span class="sub-title extra06-color">our four step
                                                process</span>
                                            <h2 class="title">Test</h2>
                                            <p class="subtitle-2">Donec metus lorem, vulputate at sapien
                                                sit amet, auctor
                                                iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit.
                                                Aliquam
                                                tristique libero at dui sodales, et placerat orci lobortis. Maecenas
                                                ipsum
                                                neque, elementum id dignissim et, imperdiet vitae mauris.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Working Process  -->

                            <!-- Start Working Process  -->
                            <div class="axil-working-process text-start mb--100 mb_md--50 mb_sm--40">
                                <div class="content order-2 order-lg-1">
                                    <div class="inner">
                                        <div class="section-title">
                                            <span class="process-step-number">4</span>
                                            <span class="sub-title extra07-color">our four step
                                                process</span>
                                            <h2 class="title">Build</h2>
                                            <p class="subtitle-2">Donec metus lorem, vulputate at sapien
                                                sit amet, auctor
                                                iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit.
                                                Aliquam
                                                tristique libero at dui sodales, et placerat orci lobortis. Maecenas
                                                ipsum
                                                neque, elementum id dignissim et, imperdiet vitae mauris.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumbnail order-1 order-lg-2">
                                    <div class="image paralax-image">
                                        <img src="assets/images/process/process-04.jpg" alt="Process Images">
                                    </div>
                                </div>
                            </div>
                            <!-- End Working Process  -->

                        </div>
                    </div>
                </div>
            </div>
            */ ?>
            <!-- End Working Process  -->

            <!-- Start Portfolio Area -->
            <?php /*
            <div class="axil-portfolio-area ax-section-gap bg-color-lightest">
                <div class="container axil-masonary-wrapper">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="section-title text-center">
                                <span class="sub-title extra07-color wow" data-splitting>our projects</span>
                                <h2 class="title wow mb--0" data-splitting>Featured Designs</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mesonry-list grid-metro3 mt--20">
                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 axil-control active portfolio-33-33">
                                    <div class="inner move-up wow">
                                        <div class="thumb paralax-image">
                                            <a href="single-case-study.html"><img src="assets/images/portfolio/portfolio-01.jpg" alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="single-case-study.html">Creative
                                                        Agency</a></h4>
                                                <span class="category">ios, design</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->

                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 axil-control portfolio-33-33">
                                    <div class="inner move-up wow">
                                        <div class="thumb paralax-image">
                                            <a href="single-case-study.html"><img src="assets/images/portfolio/portfolio-02.jpg" alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="single-case-study.html">Rant bike</a></h4>
                                                <span class="category">Branding</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->

                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 portfolio-33-33">
                                    <div class="inner move-up wow">
                                        <div class="thumb paralax-image">
                                            <a href="single-case-study.html"><img src="assets/images/portfolio/portfolio-03.jpg" alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="single-case-study.html">All Volees</a></h4>
                                                <span class="category">Web application</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->

                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 portfolio-33-33">
                                    <div class="inner move-up wow">
                                        <div class="thumb paralax-image">
                                            <a href="single-case-study.html"><img src="assets/images/portfolio/portfolio-04.jpg" alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="single-case-study.html">Larq</a></h4>
                                                <span class="category">Branding, design</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->

                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 portfolio-33-33">
                                    <div class="inner move-up wow">
                                        <div class="thumb paralax-image">
                                            <a href="single-case-study.html"><img src="assets/images/portfolio/portfolio-05.jpg" alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="single-case-study.html">Rant bike</a></h4>
                                                <span class="category">Branding</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->

                                <!-- Start Single Portfolio -->
                                <div class="portfolio portfolio_style--1 portfolio-33-33">
                                    <div class="inner move-up wow">
                                        <div class="thumb paralax-image">
                                            <a href="single-case-study.html"><img src="assets/images/portfolio/portfolio-01.jpg" alt="Portfolio Images"></a>
                                        </div>
                                        <div class="port-overlay-info">
                                            <div class="hover-action">
                                                <h4 class="title"><a href="single-case-study.html">Creative
                                                        Agency</a></h4>
                                                <span class="category">ios, design</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Portfolio -->
                            </div>
                            <div class="view-all-portfolio-button mt--60 text-center move-up wow">
                                <a class="axil-button btn-large btn-transparent" href="#"><span
                                        class="button-text">Discover
                                        More Projects</span><span class="button-icon"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            */ ?>
            <!-- End Portfolio Area -->

            <!-- Start Call To Action -->
            <div class="axil-call-to-action-area shape-position ax-section-gap theme-gradient">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="axil-call-to-action">
                                <div class="section-title text-center">
                                    <span class="sub-title extra04-color wow" data-splitting>Let's work together</span>
                                    <h2 class="title wow" data-splitting>Need a successful project?</h2>
                                    <a class="axil-button btn-large btn-transparent" href="#"><span
                                            class="button-text">Estimate Project</span><span
                                            class="button-icon"></span></a>
                                    <!-- <div class="callto-action">
                                        <span class="text wow" data-splitting>Or call us now</span>
                                        <span class="wow" data-splitting><i class="fal fa-phone-alt"></i> <a href="#">(123)
                                    456 7890</a></span>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="shape-group">
                    <div class="shape shape-01">
                        <i class="icon icon-shape-14"></i>
                    </div>
                    <div class="shape shape-02">
                        <i class="icon icon-shape-09"></i>
                    </div>
                    <div class="shape shape-03">
                        <i class="icon icon-shape-10"></i>
                    </div>
                    <div class="shape shape-04">
                        <i class="icon icon-shape-11"></i>
                    </div>
                </div>
            </div>
            <!-- End Call To Action -->

        </div>

        <!-- Start Footer Area -->
        <?php include_once 'footer.php'; ?>
        <!-- End Footer Area -->
    </div>
    <!-- JS
============================================ -->

    <?php include_once 'script.php'; ?>

</body>

</html>