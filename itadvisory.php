<!doctype html>
<html class="no-js" lang="en">

<?php include_once 'head.php'; ?>

<body>
    <div class="main-content">
        <div id="my_switcher" class="my_switcher">
            <ul>
                <li>
                    <a href="javascript: void(0);" data-theme="light" class="setColor light">
                        <img src="assets/images/about/sun-01.svg" alt="Sun images"><span title="Light Mode"> Light</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" data-theme="dark" class="setColor dark">
                        <img src="assets/images/about/vector.svg" alt="Vector Images"><span title="Dark Mode"> Dark</span>
                    </a>
                </li>
            </ul>
        </div>

        <!-- Start Header -->
        <?php include_once 'header.php'; ?>
        <!-- Start Header -->

        <!-- Start Popup Mobile Menu -->
        <?php include_once 'mobilemenu.php'; ?>
        <!-- End Popup Mobile Menu -->

        <!-- Start Sidebar Area  -->
        <?php /* include_once 'sidebar.php'; */ ?>
        <!-- End Sidebar Area  -->

        <!-- Start Page Wrapper -->
        <main class="page-wrappper">

            <!-- Start Breadcrumb Area -->
            <div class="axil-breadcrumb-area breadcrumb-style-2 pt--170 pb--70 theme-gradient">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 order-2 order-lg-1 mt_md--30 mt_sm--30">
                            <div class="inner">
                                <div class="content">
                                    <h1 class="page-title mb--20">Best solutions for your business</h1>
                                    <p class="subtitle-2">A quick view of industry specific problems solved with design
                                        by
                                        the awesome team at Keystroke.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 order-1 order-lg-2">
                            <div class="breadcrumb-thumbnail-group with-image-group text-end">
                                <div class="thumbnail">
                                    <img class="paralax-image" src="assets/images/others/keystoke-image-1.png" alt="Keystoke Images">
                                </div>
                                <div class="image-group">
                                    <img class="paralax-image" src="assets/images/others/keystoke-image-2.svg" alt="Keystoke Images">
                                </div>
                                <div class="shape-group">
                                    <div class="shape shape-1">
                                        <i class="icon icon-breadcrumb-1"></i>
                                    </div>
                                    <div class="shape shape-2">
                                        <i class="icon icon-breadcrumb-2"></i>
                                    </div>
                                    <div class="shape shape-3 customOne">
                                        <i class="icon icon-breadcrumb-3"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Breadcrumb Area -->


            <!-- Start Counterup Area -->
            <div class="axil-counterup-area ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <span class="sub-title extra08-color wow" data-splitting>experts in field</span>
                                <h2 class="title wow" data-splitting>Design startup movement</h2>
                                <p class="subtitle-2 wow" data-splitting>In vel varius turpis, non dictum sem. Aenean in
                                    efficitur ipsum, in
                                    egestas ipsum. Mauris in mi ac tellus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Start Counterup Area -->
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 move-up wow">
                            <div class="axil-counterup mt--60 text-center counter-first">
                                <div class="icon">
                                    <img src="assets/images/counterup/shape-01.png" alt="Shape Images">
                                </div>
                                <h3 class="count">15</h3>
                                <p>Years of operation</p>
                            </div>
                        </div>
                        <!-- End Counterup Area -->

                        <!-- Start Counterup Area -->
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 move-up wow">
                            <div class="axil-counterup mt--60 text-center counter-second">
                                <div class="icon">
                                    <img src="assets/images/counterup/shape-02.png" alt="Shape Images">
                                </div>
                                <h3 class="count">360</h3>
                                <p>Projects deliverd</p>
                            </div>
                        </div>
                        <!-- End Counterup Area -->

                        <!-- Start Counterup Area -->
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 move-up wow">
                            <div class="axil-counterup mt--60 mt_md--30 mt_sm--30 text-center counter-third">
                                <div class="icon">
                                    <img src="assets/images/counterup/shape-03.png" alt="Shape Images">
                                </div>
                                <h3 class="count">600</h3>
                                <p>Specialist</p>
                            </div>
                        </div>
                        <!-- End Counterup Area -->

                        <!-- Start Counterup Area -->
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 move-up wow">
                            <div class="axil-counterup mt--60 text-center counter-four">
                                <div class="icon">
                                    <img src="assets/images/counterup/shape-04.png" alt="Shape Images">
                                </div>
                                <h3 class="count">64</h3>
                                <p>Years of operation</p>
                            </div>
                        </div>
                        <!-- End Counterup Area -->
                    </div>
                </div>
            </div>
            <!-- End Counterup Area -->

            <!-- Start Service Area -->
            <div class="axil-service-area ax-section-gap bg-color-lightest">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="section-title text-start">
                                <span class="sub-title extra08-color wow" data-splitting>what we can do for you</span>
                                <h2 class="title wow" data-splitting><span>Our capabilities</span></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <!-- Start Single Service -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 move-up wow">
                            <div class="axil-service text-center axil-control paralax-image">
                                <div class="inner">
                                    <div class="icon">
                                        <div class="icon-inner">
                                            <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                            <div class="image-2"><img src="assets/images/icons/icon-04.svg" alt="Shape Images"></div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4 class="title"><a href="single-service.html">Visual branding</a></h4>
                                        <p>We design professional looking yet simple websites.
                                            Our designs are search engine
                                            and user friendly. </p>
                                        <a class="axil-button" data-hover="Learn More" href="single-service.html">Learn
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->

                        <!-- Start Single Service -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 move-up wow">
                            <div class="axil-service text-center axil-control paralax-image active">
                                <div class="inner">
                                    <div class="icon gradient-color-02">
                                        <div class="icon-inner">
                                            <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                            <div class="image-2"><img src="assets/images/icons/icon-02.svg" alt="Shape Images"></div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4 class="title"><a href="single-service.html">Interaction</a></h4>
                                        <p>We design professional looking yet simple websites.
                                            Our designs are search engine
                                            and user friendly. </p>
                                        <a class="axil-button" data-hover="Learn More" href="single-service.html">Learn
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->

                        <!-- Start Single Service -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 move-up wow">
                            <div class="axil-service text-center axil-control paralax-image">
                                <div class="inner">
                                    <div class="icon gradient-color-03">
                                        <div class="icon-inner">
                                            <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                            <div class="image-2"><img src="assets/images/icons/icon-01.svg" alt="Shape Images"></div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4 class="title"><a href="single-service.html">Creative production</a></h4>
                                        <p>We design professional looking yet simple websites.
                                            Our designs are search engine
                                            and user friendly. </p>
                                        <a class="axil-button" data-hover="Learn More" href="single-service.html">Learn
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->

                        <!-- Start Single Service -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 move-up wow">
                            <div class="axil-service text-center axil-control paralax-image">
                                <div class="inner">
                                    <div class="icon gradient-color-04">
                                        <div class="icon-inner">
                                            <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                            <div class="image-2"><img src="assets/images/icons/icon-03.svg" alt="Shape Images"></div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4 class="title"><a href="single-service.html">Development</a></h4>
                                        <p>We design professional looking yet simple websites.
                                            Our designs are search engine
                                            and user friendly. </p>
                                        <a class="axil-button" data-hover="Learn More" href="single-service.html">Learn
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->

                        <!-- Start Single Service -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 move-up wow">
                            <div class="axil-service text-center axil-control paralax-image">
                                <div class="inner">
                                    <div class="icon gradient-color-06">
                                        <div class="icon-inner">
                                            <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                            <div class="image-2"><img src="assets/images/icons/icon-02.svg" alt="Shape Images"></div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4 class="title"><a href="single-service.html">Digital marketing</a></h4>
                                        <p>We design professional looking yet simple websites.
                                            Our designs are search engine
                                            and user friendly. </p>
                                        <a class="axil-button" data-hover="Learn More" href="single-service.html">Learn
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->

                        <!-- Start Single Service -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 move-up wow">
                            <div class="axil-service text-center axil-control paralax-image">
                                <div class="inner">
                                    <div class="icon gradient-color-05">
                                        <div class="icon-inner">
                                            <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                            <div class="image-2"><img src="assets/images/icons/icon-02.svg" alt="Shape Images"></div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h4 class="title"><a href="single-service.html">SEO optimization</a></h4>
                                        <p>We design professional looking yet simple websites.
                                            Our designs are search engine
                                            and user friendly. </p>
                                        <a class="axil-button" data-hover="Learn More" href="single-service.html">Learn
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->

                    </div>
                </div>
            </div>
            <!-- End Service Area -->

            <!-- Start Our Service Area  -->
            <div class="axil-service-area ax-section-gap bg-color-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-xl-8">
                            <div class="section-title text-start">
                                <span class="sub-title extra04-color wow" data-splitting>process</span>
                                <h2 class="title wow" data-splitting>This is our way</h2>
                                <p class="subtitle-2">In vel varius turpis, non dictum sem. Aenean in efficitur ipsum,
                                    in
                                    egestas ipsum. Mauris in mi ac tellus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-12 mt--50 mt_md--40 mt_sm--30 move-up wow">
                            <div class="axil-service-style--3">
                                <div class="icon">
                                    <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                    <div class="text">1</div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="single-service.html">1. Discuss</a></h4>
                                    <p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel
                                        hendrerit nisi. Vestibulum eget risus velit.</p>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-12 mt--50 mt_md--40 mt_sm--30 move-up wow">
                            <div class="axil-service-style--3 color-var--2">
                                <div class="icon">
                                    <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                    <div class="text">2</div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="single-service.html">2. Mapping</a></h4>
                                    <p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel
                                        hendrerit nisi. Vestibulum eget risus velit.</p>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-12 mt--50 mt_md--40 mt_sm--30 move-up wow">
                            <div class="axil-service-style--3 color-var--3">
                                <div class="icon">
                                    <img src="assets/images/icons/layer.svg" alt="Icon Images">
                                    <div class="text">3</div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="single-service.html">3. Execution</a></h4>
                                    <p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel
                                        hendrerit nisi. Vestibulum eget risus velit.</p>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service  -->
                    </div>
                </div>
            </div>
            <!-- End Our Service Area  -->

            <!-- Start Call To Action -->
            <div class="axil-call-to-action-area shape-position ax-section-gap theme-gradient">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="axil-call-to-action">
                                <div class="section-title text-center">
                                    <span class="sub-title extra04-color wow" data-splitting>Let's work together</span>
                                    <h2 class="title wow" data-splitting>Need a successful project?</h2>
                                    <a class="axil-button btn-large btn-transparent" href="#"><span
                                class="button-text">Estimate Project</span><span class="button-icon"></span></a>
                                    <!-- <div class="callto-action">
                                        <span class="text wow" data-splitting>Or call us now</span>
                                        <span class="wow" data-splitting><i class="fal fa-phone-alt"></i> <a href="#">(123)
                                    456 7890</a></span>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shape-group">
                    <div class="shape shape-01">
                        <i class="icon icon-shape-14"></i>
                    </div>
                    <div class="shape shape-02">
                        <i class="icon icon-shape-09"></i>
                    </div>
                    <div class="shape shape-03">
                        <i class="icon icon-shape-10"></i>
                    </div>
                    <div class="shape shape-04">
                        <i class="icon icon-shape-11"></i>
                    </div>
                </div>
            </div>
            <!-- End Call To Action -->

        </main>
        <!-- End Page Wrapper -->

        <!-- Start Footer Area -->
        <?php include_once 'footer.php'; ?>
        <!-- End Footer Area -->
    </div>
    <!-- JS
============================================ -->

    <?php include_once 'script.php'; ?>

</body>

</html>